#ifndef AspFast_H
#define AspFast_H

#include <valarray>
#include <pluginmethod.h>

#include "OpenFAST.H"

class AspFast : public PluginMethod
{

private:
    bool active; // aspfast enabled/disabled

    fast::fastInputs fi;
    fast::OpenFAST *FAST;

    std::string inputFile; // aspfast input
    double fastTime;

    // GRASP variables
    //
    // x, y, z cell center and face center coordinates (full and half level)
    float *xf;
    float *yf;
    float *zf;

    float *xh;
    float *yh;
    float *zh;

    // Velocity
    float *u;
    float *v;
    float *w;

    // Body force induced velocity tendency
    float *ubf;
    float *vbf;
    float *wbf;

    // These are for visualization purposes
    float *fastId;        // For debugging purposes
    float *fastSpreading; // source term = spreading * force
    float *fastForce;     // magnitude of the source term

    // Read from GRASP
    //
    // Number of cells in x, y, z direction
    int ni;
    int nj;
    int nk;

    // Cell size in x, y, z direction
    double dx;
    double dy;
    float *dz; // Can have stretching -> array of cell sizes

    std::vector<int> bodyForceExtent; // [imin, imax, jmin, jmax, kmin, kmax]

    // Coupling variables
    //
    double tstart = -0.0;
    bool twoWayCoupling;    // Project forces to GRASP?
    bool filteredALM;       // Use the filtered ALM (see Martinez in JFM (2019))
    bool feedVelAtForcePts; // Sets if one uses setVelocity() or setVelocityForceNode() from the Fast API
    bool limitProjRange;    // Set if the projection of the body force should be truncated after a threshold

    // Arrays needed for the filtered ALM (in order to store data from previous time steps)
    std::vector<std::valarray<double>> bladeForceVecDist;    // Force vector for t=n-1
    std::vector<std::valarray<double>> bladeLiftVecDist;     // Lift vector for t=n-1
    std::vector<std::valarray<double>> bladeGradLiftVecDist; // lift gradient vector for t=n-1

    std::vector<std::valarray<double>> deltaUyVec;      // vectorial velocity correction for t=n-1
    std::vector<std::valarray<double>> deltaUyVecStash; // vectorial velocity correction for t=n-2
    std::vector<std::valarray<double>> uyLesVec;        // induced veleocity uyLes t=n-1
    std::vector<std::valarray<double>> uyOptVec;        // induced veleocity uyOpt t=n-1

    std::vector<std::valarray<double>> uInfRelVec; // the corrected LES velocity relative to the moving blade (needed for equ. 5.3)
    std::vector<double> uInfRelMagn;               // the magnitude of the corrected LES velocity relative to the moving blade (needed for equ. 5.7)

    std::vector<double> localChordDist; // Local chord lengths (assuming all blades/turbines are equal!)

    // Project rotor, nacelle, tower to GRASP if twoWayCoupling=true?
    bool includeRotorForce;
    bool includeNacelleForce;
    bool includeTowerForce;

    // How far upwind is the velocity sampled from the node?
    // Upwind direction is determined from the shaft direction projected on xy plane
    double nacelleSamplingDistance;
    double towerSamplingDistance;
    bool move_bbox = false;

    double airDensity;
    double epsilon;                 // Gaussian width
    double uniformGaussianConstant; // Calculated from Gaussian width
    double projectionRadius;        // Projection distance, calculated from Gaussian width

    std::string velocityInterpolation; // linear, nearest neighbor. TODO change to enum

    // To calculate nacelle force = 0.5*nacelleVelocity^2*nacelleCd*nacelleArea
    double nacelleCd;
    double nacelleArea;
    std::vector<double> nacelleVelocity;

    // The source term calculated at the cell centers and on GRASP grid
    std::vector<std::valarray<double>> bodyForce;

    // For statistics
    float dtav = -1.0;
    float dtwrite = -1.0;
    float tnextav = -1.0;
    float tnextwrite = -1.0;
    int nSamples = 0;
    std::vector<std::string> ncstat_varnames = {
        "x_FAST", "y_FAST", "z_FAST",
        "fx_FAST", "fy_FAST", "fz_FAST",
        "uw_FAST", "vw_FAST", "ww_FAST",
        "ur_FAST", "vr_FAST", "wr_FAST",
        "orientation0_FAST", "orientation1_FAST", "orientation2_FAST",
        "orientation3_FAST", "orientation4_FAST", "orientation5_FAST",
        "orientation6_FAST", "orientation7_FAST", "orientation8_FAST",
        "deltaUyCorr_x", "deltaUyCorr_y", "deltaUyCorr_z",
        "uIndLes_x", "uIndLes_y", "uIndLes_z",
        "uIndOpt_x", "uIndOpt_y", "uIndOpt_z",
        "liftCorr_x", "liftCorr_y", "liftCorr_z",
        "uInfBlade_x", "uInfBlade_y", "uInfBlade_z"};

    /*
     std::vector<std::string> ncstat_varnames = {
         "x_FAST", "y_FAST", "z_FAST",
         "fx_FAST", "fy_FAST", "fz_FAST",
         "uw_FAST", "vw_FAST", "ww_FAST",
         "ur_FAST", "vr_FAST", "wr_FAST",
         "orientation0_FAST", "orientation1_FAST", "orientation2_FAST",
         "orientation3_FAST", "orientation4_FAST", "orientation5_FAST",
         "orientation6_FAST", "orientation7_FAST", "orientation8_FAST"
     };
     */

    NcWriter *ncwriter;

    // Private functions
    //
    void initializeGraspArrays();
    void readInputFile();
    inline bool checkFileExists(const std::string &name);
    void readTurbineData(int iTurb, fast::fastInputs &fi, YAML::Node turbNode);

    // GRASP arrays
    // read from GPU to CPU and write from CPU to GPU
    void readGraspArrays();
    void writeGraspArrays();

    // Main functions of the coupling
    // Velocity sampling
    void sampleVelocity(int turbineI);
    void sampleVelocityForceNodes(int turbineI);
    std::vector<double> calcVelocity(const std::vector<double> &point, std::string method = "nearest neighbor");

    // Body force projection
    void compute_bodyforce_boundingbox();
    void calcBodyForce(int turbineI);
    void uniformBodyForce(const std::vector<double> &point, const std::vector<double> &force);
    void diskBodyForce(const std::vector<double> &point, const std::vector<double> &force, std::vector<double> &axialVec, double r0);
    void bodyForce2GraspGrid();

    // Filtered ALM
    void calcFilteredAlmVelCorr(int turbineI); // Main function: Calculates induced velocity uy

    // Velocity and its gradient at the cell center
    std::vector<double> Ucenter(int i, int j, int k);
    std::vector<double> gradU(int i, int j, int k);

    void initializeOutput();
    void writeOutput(int turbineId);
    void stat_wadd(double *average, const double *sample, const unsigned int N);

    // helper functions
    // i, j, k index of the nearest cell
    inline std::vector<int> nearestIJK(const std::vector<double> &point, float *x, float *y, float *z);

    // Global index of the nearest cell
    inline int nearestIndex(const std::vector<double> &point, float *x, float *y, float *z);

    // Index of the nearest cell in 1D array
    inline int nearestIndex1(float *x, float loc, int n);

    // Global index from i, j, k
    inline int idx(int i, int j, int k);

    // Global index from ijk array
    inline int idx(std::vector<int> ijk);

public:
    AspFast(SimulationData *);
    ~AspFast();

    virtual void init() override;
    virtual void doit() override;

    void end();
};

#endif
