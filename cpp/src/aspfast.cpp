#include <iostream>
#include "yaml-cpp/yaml.h"
#include <omp.h>
#include <cassert>

#include <ncwriter.h>
#include <simulationdata.h>
#include "aspfast.h"
#include "utils.h"

// Required for Aspire plugin
extern "C" void call_constructor(PluginMethod **pluginMethod, SimulationData *simdata)
{
    *pluginMethod = new AspFast(simdata);
}
extern "C" void call_destructor(PluginMethod *pluginMethod) { delete pluginMethod; }

// Constructor
AspFast::AspFast(SimulationData *_simulationData) : PluginMethod(_simulationData),
                                                    active(true),
                                                    FAST(new fast::OpenFAST),
                                                    fastTime(0)
{
}

// Destructor
AspFast::~AspFast()
{
    FAST->end();
    delete FAST;
}

// Aspire calls this at the beginning of the simulation.
void AspFast::init()
{
    // Check if aspfast input file exists for this experiment number
    std::string experimentNumber = simdata->get_scalar("exp_string").as<std::string>();
    inputFile = simdata->get_scalar("nml_dir").as<std::string>() + "aspfast." + experimentNumber + ".yml";
    if (!checkFileExists(inputFile))
    {
        std::cout << inputFile << " not found. AspFast disabled" << std::endl;
        active = false;
        return;
    }

    // init MPI
    int nProcs;
    int rank;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    std::cout << "RANK " << rank << std::endl;

    // Read aspfast input file and fill the fi fastInputs container
    readInputFile();

    // Initialize OpenFast interface
    FAST->setInputs(fi);
    FAST->allocateTurbinesToProcsSimple();
    FAST->init();

    // Initialize GRASP
    initializeGraspArrays();

    // Initialize output
    initializeOutput();

    // Initialize bodyforce, projection constant and projection radius
    bodyForce = std::vector<std::valarray<double>>(ni * nj * nk, std::valarray<double>(0.0, 3));
    uniformGaussianConstant = 1.0 / (pow(epsilon, 3) * pow(M_PI, 1.5));

//    // Radius where the Gaussian projection has a value of 0.1%
//    projectionRadius = epsilon * sqrt(log(1.0 / 0.001));
    // Radius in which 99% of the weight lies
    //projectionRadius = epsilon * sqrt(2) * inverf(.99)
    projectionRadius = epsilon * 2.575829303548901; // computed in python

    // Compute extent of the bodyforce bounding box
    compute_bodyforce_boundingbox();

    // Initialize arrays for filtered ALM
    if (filteredALM)
    {
        int forcePtsTotal = 0;
        for (int turbineId = 0; turbineId < fi.nTurbinesGlob; ++turbineId)
        {
            int nBld = FAST->get_numBlades(turbineId);
            int nBldPts = FAST->get_numForcePtsBlade(turbineId);
            forcePtsTotal += nBld * nBldPts;
        }

        // Arrays for lift G
        bladeForceVecDist = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3));    // Force vector for t=n-1
        bladeLiftVecDist = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3));     // Lift vector for t=n-1
        bladeGradLiftVecDist = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3)); // Lift gradient vector for t=n-1

        // Arrays for LES, Blade, Corrected Velocities
        deltaUyVec = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3));      // vectorial velocity correction for t=n-1
        deltaUyVecStash = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3)); // vectorial velocity correction for t=n-2 (needed for computation of deltaUyVec via relaxation (equ. 5.8))
        uyLesVec = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3));        // induced veleocity uyLes t=n-1
        uyOptVec = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3));        // induced veleocity uyOpt t=n-1
        uInfRelVec = std::vector<std::valarray<double>>(forcePtsTotal, std::valarray<double>(0.0, 3));      // the corrected LES velocity relative to the moving blade
        uInfRelMagn = std::vector<double>(forcePtsTotal, 0.0);                                              // the magnitude of the corrected LES velocity relative to the moving blade (needed for equ. 5.7)

        // Store local chord lengths (assume that all turbines are the same! TODO: generalise)
        int forcePtsT1 = FAST->get_numForcePtsBlade(0);
        localChordDist = std::vector<double>(forcePtsT1, 0.0);
        for (int i = 0; i < forcePtsT1; ++i)
        {
            localChordDist[i] = FAST->getChord(i + 1, 0); // TODO: is the nacelle node actually included?!
            std::cout << "local chord length " << localChordDist[i] << std::endl;
        }
    }

    // OpenFAST supports only constant time-step. GRASP timestep should be a multiplier of dtFAST
    double dtGrasp = simdata->get_scalar("dtmax").as<double>();
    if (dtGrasp < fi.dtFAST)
    {
        std::cout << "WARNING: dt_grasp < dt_fast, dt_grasp/dt_fast = " << dtGrasp / fi.dtFAST << std::endl;
    }
    if (std::fmod(dtGrasp, fi.dtFAST) > 1e-6)
    {
        std::cout << "WARNING: GRASP timestep should be a multiplier of OpenFAST timestep, dt_grasp \% dt_fast = " << std::fmod(dtGrasp, fi.dtFAST) << std::endl;
    }

    if (twoWayCoupling && includeNacelleForce && nacelleSamplingDistance < 1e-6)
    {
        std::cout << "WARNING: nacelleSamplingDistance = " << nacelleSamplingDistance << std::endl;
    }
    if (twoWayCoupling && includeTowerForce && towerSamplingDistance < 1e-6)
    {
        std::cout << "WARNING: towerSamplingDistance = " << towerSamplingDistance << std::endl;
    }

    assert(dtwrite >= dtav);

    std::cout << "initialized " << inputFile << std::endl;

    std::cout << std::boolalpha;
    std::cout << "two way coupling:          " << twoWayCoupling << std::endl;
    std::cout << "filtered ALM:              " << filteredALM << std::endl;
    std::cout << "limit force projection range: " << limitProjRange << std::endl;
    std::cout << "feedVelAtForcePts:         " << feedVelAtForcePts << std::endl;
    std::cout << "dtFAST:                    " << fi.dtFAST << std::endl;
    std::cout << "dtGrasp:                   " << dtGrasp << std::endl;
    std::cout << "epsilon:                   " << epsilon << std::endl;
    std::cout << "Velocity interpolation:    " << velocityInterpolation << std::endl;
    std::cout << "Number of turbines:        " << fi.nTurbinesGlob << std::endl;

    if (twoWayCoupling)
    {
        std::cout << "Include rotor force:       " << includeRotorForce << std::endl;
        std::cout << "Include nacelle force:     " << includeNacelleForce << std::endl;
        std::cout << "Include tower force:       " << includeTowerForce << std::endl;
        if (includeNacelleForce)
        {
            std::cout << "Nacelle sampling distance: " << nacelleSamplingDistance << std::endl;
            std::cout << "Nacelle Cd                 " << nacelleCd << std::endl;
            std::cout << "Nacelle area               " << nacelleArea << std::endl;
        }
        if (includeTowerForce)
        {
            std::cout << "Tower sampling distance:   " << towerSamplingDistance << std::endl;
        }
    }
}

// Aspire calls this at each time step
void AspFast::doit()
{
    if (!active)
        return;

    double time = simdata->get_scalar("timee").as<double>();

    if (time < tstart)
        return;

    int rkstage = simdata->get_scalar("stage").as<int>();

    // Calculate body force only at the first stage and use the same force on second and third stage.
    if ((rkstage != 0) && twoWayCoupling)
    {
        simdata->get_variable("up").add(simdata->get_variable("ubf"));
        simdata->get_variable("vp").add(simdata->get_variable("vbf"));
        simdata->get_variable("wp").add(simdata->get_variable("wbf"));
        return;
    }

    std::cout << " Time: " << time << "/" << simdata->get_scalar("runtime").as<double>() << std::endl;

    // Read velocity and velocity tendency from Grasp
    readGraspArrays();

// Update velocity to FAST
#pragma omp parallel for
    for (int i = 0; i < fi.nTurbinesGlob; ++i)
{
        if (feedVelAtForcePts)
        {
            sampleVelocityForceNodes(i);

            // Filtered ALM: Compute velocity correction
            if (filteredALM)
            {
                calcFilteredAlmVelCorr(i);
            }
        }
        else
    {
        sampleVelocity(i);
    }
    }

    if (FAST->isTimeZero())
    {
        FAST->solution0();
    }

    // Calculate how many substeps
    int fastSteps = (time - tstart - fastTime) / fi.dtFAST + 0.5;
    // Time stepping for OpenFAST
    for (int i = 0; i < fastSteps; ++i)
        // FAST->stepNoWrite(); // No restart files
        FAST->step();
    fastTime += fastSteps * fi.dtFAST;

    if (twoWayCoupling)
    {
        if (move_bbox)
            compute_bodyforce_boundingbox();

        std::fill(bodyForce.begin(), bodyForce.end(), std::valarray<double>(0.0, 3));
#pragma omp parallel for
        for (int i = 0; i < fi.nTurbinesGlob; ++i)
        {
            calcBodyForce(i);
        }
        bodyForce2GraspGrid();
        // writeGraspArrays();
    }

    if ((dtav > 0) && (time > tnextav))
    {
        while (time >= tnextav)
        {
            tnextav += dtav;
        }
#pragma omp parallel for
        for (int i = 0; i < fi.nTurbinesGlob; ++i)
        {
            writeOutput(i);
        }

        nSamples++;
        if (time >= tnextwrite)
        {
            while (time >= tnextwrite)
            {
                tnextwrite += dtwrite;
            }
            ncwriter->append_variables(time);
            nSamples = 0;
        }
    }
}

void AspFast::end()
{
    MPI_Finalize();
}

void AspFast::initializeGraspArrays()
{
    ASP::Coordinate &xff = simdata->get_coordinate("xf");
    ASP::Coordinate &yff = simdata->get_coordinate("yf");
    ASP::Coordinate &zff = simdata->get_coordinate("zf");
    ASP::Coordinate &xhh = simdata->get_coordinate("xh");
    ASP::Coordinate &yhh = simdata->get_coordinate("yh");
    ASP::Coordinate &zhh = simdata->get_coordinate("zh");

    xf = xff.get_cpu_ptr<float>();
    yf = yff.get_cpu_ptr<float>();
    zf = zff.get_cpu_ptr<float>();
    xh = xhh.get_cpu_ptr<float>();
    yh = yhh.get_cpu_ptr<float>();
    zh = zhh.get_cpu_ptr<float>();

    u = simdata->get_variable("u").get_cpu_ptr<float>();
    v = simdata->get_variable("v").get_cpu_ptr<float>();
    w = simdata->get_variable("w").get_cpu_ptr<float>();

    ni = simdata->get_scalar("nx").as<int>();
    nj = simdata->get_scalar("ny").as<int>();
    nk = simdata->get_scalar("nz").as<int>();

    dx = simdata->get_scalar("dx").as<float>();
    dy = simdata->get_scalar("dy").as<float>();

    // z-direction can have stretching
    ASP::Variable &dzt = simdata->get_variable("dzf");
    dzt.gpu2cpu();
    dz = dzt.get_cpu_ptr<float>();

    // If epsilon is not defined
    if (epsilon <= 0)
        epsilon = 2 * dx;

    simdata->add_variable("ubf", "m/s2", "FAST force x", simdata->get_grid("c"), ASP::Type::FLOAT);
    simdata->add_variable("vbf", "m/s2", "FAST force y", simdata->get_grid("c"), ASP::Type::FLOAT);
    simdata->add_variable("wbf", "m/s2", "FAST force z", simdata->get_grid("c"), ASP::Type::FLOAT);

    ubf = simdata->get_variable("ubf").get_cpu_ptr<float>();
    vbf = simdata->get_variable("vbf").get_cpu_ptr<float>();
    wbf = simdata->get_variable("wbf").get_cpu_ptr<float>();

    simdata->add_variable("fastId", "", "FAST cell type", simdata->get_grid("c"), ASP::Type::FLOAT); // 0 = No force, 1 Nacelle, 2 Tower, 3 Blade. TODO not functional
    simdata->add_variable("fastSpreading", "", "FAST spreading", simdata->get_grid("c"), ASP::Type::FLOAT);
    simdata->add_variable("fastForce", "m/s2", "FAST force", simdata->get_grid("c"), ASP::Type::FLOAT);

    fastId = simdata->get_variable("fastId").get_cpu_ptr<float>();
    fastSpreading = simdata->get_variable("fastSpreading").get_cpu_ptr<float>();
    fastForce = simdata->get_variable("fastForce").get_cpu_ptr<float>();
}

inline bool AspFast::checkFileExists(const std::string &name)
{
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}
void AspFast::readTurbineData(int iTurb, fast::fastInputs &fi, YAML::Node turbNode)
{
    fi.globTurbineData[iTurb].TurbID = iTurb; // turbNode["turb_id"].as<int>();

    std::string experimentNumber = simdata->get_scalar("exp_string").as<std::string>();

    fi.globTurbineData[iTurb].FASTInputFileName = simdata->get_scalar("nml_dir").as<std::string>() + turbNode["FAST_input_filename"].as<std::string>();
    fi.globTurbineData[iTurb].FASTRestartFileName = simdata->get_scalar("nml_dir").as<std::string>() + turbNode["restart_filename"].as<std::string>();
    if (turbNode["turbine_base_pos"].IsSequence())
    {
        fi.globTurbineData[iTurb].TurbineBasePos = turbNode["turbine_base_pos"].as<std::vector<double>>();
    }
    if (turbNode["turbine_hub_pos"].IsSequence())
    {
        fi.globTurbineData[iTurb].TurbineHubPos = turbNode["turbine_hub_pos"].as<std::vector<double>>();
    }
    fi.globTurbineData[iTurb].numForcePtsBlade = turbNode["num_force_pts_blade"].as<int>();
    fi.globTurbineData[iTurb].numForcePtsTwr = turbNode["num_force_pts_tower"].as<int>();
    if (turbNode["nacelle_cd"])
    {
        fi.globTurbineData[iTurb].nacelle_cd = turbNode["nacelle_cd"].as<float>();
        nacelleCd = turbNode["nacelle_cd"].as<float>();
    }
    if (turbNode["nacelle_area"])
    {
        fi.globTurbineData[iTurb].nacelle_area = turbNode["nacelle_area"].as<float>();
        nacelleArea = turbNode["nacelle_area"].as<float>();
    }
    std::cout << iTurb << " "
              << " " << fi.globTurbineData[iTurb].TurbID << " " << fi.globTurbineData[iTurb].TurbineBasePos[0] << " " << fi.globTurbineData[iTurb].TurbineBasePos[1] << " " << fi.globTurbineData[iTurb].TurbineBasePos[2] << std::endl;
}

void AspFast::readInputFile()
{
    fi.comm = MPI_COMM_WORLD;

    if (checkFileExists(inputFile))
    {

        YAML::Node cDriverInp = YAML::LoadFile(inputFile);

        twoWayCoupling = cDriverInp["twoWayCoupling"].as<bool>(true);
        tstart = cDriverInp["tstart"].as<float>(0);
        filteredALM = cDriverInp["filteredALM"].as<bool>(true);
        feedVelAtForcePts = cDriverInp["feedVelAtForcePts"].as<bool>(true);
        limitProjRange = cDriverInp["limitProjRange"].as<bool>(true);

        includeRotorForce = cDriverInp["includeRotorForce"].as<bool>(true);
        includeNacelleForce = cDriverInp["includeNacelleForce"].as<bool>(true);
        includeTowerForce = cDriverInp["includeTowerForce"].as<bool>(true);

        nacelleSamplingDistance = cDriverInp["nacelleSamplingDistance"].as<double>(0);
        towerSamplingDistance = cDriverInp["towerSamplingDistance"].as<double>(0);
        move_bbox = cDriverInp["move_bbox"].as<bool>(false);

        airDensity = cDriverInp["airDensity"].as<double>(1.2);
        epsilon = cDriverInp["epsilon"].as<double>(0);
        velocityInterpolation = cDriverInp["velocityInterpolation"].as<std::string>("linear");
        dtav = cDriverInp["dtav"].as<float>(-1);
        dtwrite = cDriverInp["dtwrite"].as<float>(-1);

        fi.nTurbinesGlob = cDriverInp["nTurbinesGlob"].as<int>();

        if (fi.nTurbinesGlob > 0)
        {

            if (cDriverInp["dryRun"])
            {
                fi.dryRun = cDriverInp["dryRun"].as<bool>();
            }

            if (cDriverInp["debug"])
            {
                fi.debug = cDriverInp["debug"].as<bool>();
            }

            if (cDriverInp["simStart"])
            {
                if (cDriverInp["simStart"].as<std::string>() == "init")
                {
                    fi.simStart = fast::init;
                }
                else if (cDriverInp["simStart"].as<std::string>() == "trueRestart")
                {
                    fi.simStart = fast::trueRestart;
                }
                else if (cDriverInp["simStart"].as<std::string>() == "restartDriverInitFAST")
                {
                    fi.simStart = fast::restartDriverInitFAST;
                }
                else
                {
                    throw std::runtime_error("simStart is not well defined in the input file");
                }
            }
            fi.tStart = cDriverInp["tStart"].as<double>(0);
            fi.nEveryCheckPoint = cDriverInp["nEveryCheckPoint"].as<int>();
            //            fi.dtFAST = cDriverInp["dtFAST"].as<double>(simdata->get_scalar("dtmax").as<double>());
            fi.dtFAST = cDriverInp["dtFAST"].as<double>();

            fi.tMax = cDriverInp["tMax"].as<double>(simdata->get_scalar("runtime").as<double>());
            // if(cDriverInp["superController"]) {
            //     fi.scStatus = cDriverInp["superController"].as<bool>();
            //     fi.scLibFile = cDriverInp["scLibFile"].as<std::string>();
            //     fi.numScInputs = cDriverInp["numScInputs"].as<int>();
            //     fi.numScOutputs = cDriverInp["numScOutputs"].as<int>();
            // }

            fi.globTurbineData.resize(fi.nTurbinesGlob);
            for (int iTurb = 0; iTurb < fi.nTurbinesGlob; iTurb++)
            {
                if (cDriverInp["Turbine" + std::to_string(iTurb)])
                {
                    readTurbineData(iTurb, fi, cDriverInp["Turbine" + std::to_string(iTurb)]);
                }
                else
                {
                    throw std::runtime_error("Node for Turbine" + std::to_string(iTurb) + " not present in input file or I cannot read it");
                }
            }
        }
        else
        {
            throw std::runtime_error("Number of turbines <= 0 ");
        }
    }
    else
    {
        throw std::runtime_error("Input file " + inputFile + " does not exist or I cannot access it");
    }
}

void AspFast::readGraspArrays()
{
    simdata->get_variable("u").gpu2cpu();
    simdata->get_variable("v").gpu2cpu();
    simdata->get_variable("w").gpu2cpu();
}

void AspFast::writeGraspArrays()
{
    simdata->get_variable("ubf").cpu2gpu();
    simdata->get_variable("vbf").cpu2gpu();
    simdata->get_variable("wbf").cpu2gpu();

    simdata->get_variable("fastId").cpu2gpu();
    simdata->get_variable("fastSpreading").cpu2gpu();
    simdata->get_variable("fastForce").cpu2gpu();
}

void AspFast::sampleVelocity(int turbineId)
{
    std::vector<double> point(3, 0.0);
    std::vector<double> velocity(3, 0.0);
    std::vector<double> shaftOrientation(3, 0.0);

    FAST->getHubShftDir(shaftOrientation, turbineId);
    int nBlades = FAST->get_numBlades(turbineId);
    int nBladePoints = FAST->get_numVelPtsBlade(turbineId);
    int nTowerPoints = FAST->get_numVelPtsTwr(turbineId);

    // std::cout << "T" << turbineId << " Shaft orientation: " << shaftOrientation[0] << " " << shaftOrientation[1] << " " << shaftOrientation[2] << std::endl;

    int turbineStartId = 0;
    for (int i = 0; i < turbineId; ++i)
    {
        turbineStartId += FAST->get_numVelPts(i);
    }

    int nodeId = 0;

    FAST->getVelNodeCoordinates(point, turbineStartId + nodeId, turbineId);
    point = sum(point, multiply(nacelleSamplingDistance, shaftOrientation));
    velocity = calcVelocity(point, velocityInterpolation);
    FAST->setVelocity(velocity, turbineStartId + nodeId, turbineId);
    nacelleVelocity = velocity;

    nodeId += 1;

    for (int j = 0; j < nBlades; ++j)
    {
        for (int k = 0; k < nBladePoints; ++k)
        {
            FAST->getVelNodeCoordinates(point, turbineStartId + nodeId, turbineId);
            velocity = calcVelocity(point, velocityInterpolation);
            FAST->setVelocity(velocity, turbineStartId + nodeId, turbineId);
            nodeId += 1;
            // std::cout << point[0] << " " << point[1] << " " << point[2] << " " << velocity[0] << " " << velocity[1] << " " << velocity[2] << std::endl;
        }
    }

    for (int k = 0; k < nTowerPoints; ++k)
    {
        FAST->getVelNodeCoordinates(point, turbineStartId + nodeId, turbineId);
        point = sum(point, multiply(towerSamplingDistance, shaftOrientation));
        velocity = calcVelocity(point, velocityInterpolation);
        FAST->setVelocity(velocity, turbineStartId + nodeId, turbineId);
        nodeId += 1;
    }
}

void AspFast::sampleVelocityForceNodes(int turbineId)
{
    std::vector<double> point(3, 0.0);
    std::vector<double> velocity(3, 0.0);
    std::vector<double> velocityCorr(3, 0.0);
    std::vector<double> velocityRel(3, 0.0);
    std::vector<double> uInfRel(3, 0.0);
    std::vector<double> deltaUy(3, 0.0);
    std::vector<double> uyLes(3, 0.0);

    std::vector<double> shaftOrientation(3, 0.0);

    FAST->getHubShftDir(shaftOrientation, turbineId);
    int nBlades = FAST->get_numBlades(turbineId);
    int nBladePoints = FAST->get_numForcePtsBlade(turbineId);
    int nTowerPoints = FAST->get_numForcePtsTwr(turbineId);
    // std::cout << "T" << turbineId << " Shaft orientation: " << shaftOrientation[0] << " " << shaftOrientation[1] << " " << shaftOrientation[2] << std::endl;

    int turbineStartId = 0;
    for (int i = 0; i < turbineId; ++i)
    {
        turbineStartId += FAST->get_numForcePts(i);
    }

    // turbine start ID excluding tower nodes (for filtered ALM arrays)
    int turbineStartIdBldOnly = 0;
    if (filteredALM)
    {
        for (int i = 0; i < turbineId; ++i)
        {
            // Calculate indices for the filtered ALM arrays (those only include blade points!)
            int nBld = FAST->get_numBlades(i);
            int nBldPts = FAST->get_numForcePtsBlade(i);
            turbineStartIdBldOnly += nBld * nBldPts;
        }
    }

    // Nacelle
    int nodeId = 0;

    FAST->getForceNodeCoordinates(point, nodeId, turbineId);
    point = sum(point, multiply(nacelleSamplingDistance, shaftOrientation));
    velocity = calcVelocity(point, velocityInterpolation);
    FAST->setVelocityForceNode(velocity, turbineStartId + nodeId, turbineId);
    nacelleVelocity = velocity;

    // Blades
    nodeId += 1;

    for (int j = 0; j < nBlades; ++j)
    {
        for (int k = 0; k < nBladePoints; ++k)
        {
            // get force node coordinate from fast and interpolate LES velocity to this point
            FAST->getForceNodeCoordinates(point, nodeId, turbineId);
            velocity = calcVelocity(point, velocityInterpolation);
            // std::cout << " ActPointVel: " << velocity[0] << " " << velocity[1] << " "<< velocity[2] << std::endl;

            // If no correction is applied directly send the velocity to fast. Otherwise add the filtered ALM velocity correction.
            if (filteredALM)
            {
                // Get the filtered ALM correction dUy and the resolved induced LES velocity for the current point
                for (int m = 0; m < 3; m++)
                {
                    deltaUy[m] = deltaUyVec[turbineStartIdBldOnly + nodeId - 1][m];
                    uyLes[m] = uyLesVec[turbineStartIdBldOnly + nodeId - 1][m];
                }
                // std::cout << " DeltaUy: " << deltaUy[0] << " " << deltaUy[1] << " "<< deltaUy[2] << std::endl;
                // std::cout << " uyLes: " << uyLes[0] << " " << uyLes[1] << " "<< uyLes[2] << std::endl;

                // JFM 2019 (Equ. 5.9): Correct the sampled velocity with "subgrid" component
                for (int m = 0; m < 3; m++)
                {
                    velocityCorr[m] = velocity[m] + deltaUy[m];
                }

                // Send the corrected LES velcoity to fast
                FAST->setVelocityForceNode(velocityCorr, turbineStartId + nodeId, turbineId);
                // std::cout << " velocityCorr: " << velocityCorr[0] << " " << velocityCorr[1] << " "<< velocityCorr[2] << std::endl;

                // Get the new relative velocity at the blade node from fast (this includes the velocity component due to body movement)
                FAST->getRelativeVelForceNode(velocityRel, turbineStartId + nodeId, turbineId);
                // std::cout << " velocityRel: " << velocityRel[0] << " " << velocityRel[1] << " "<< velocityRel[2] << std::endl;

                // (Equ. 5.1): Obtain free stream velocity (LES velocity corrected for induced velocity component which is resolved by LES)
                // This velocity is needed to compute the lift component of the total force from fast
                for (int m = 0; m < 3; m++)
                {
                    uInfRel[m] = velocityRel[m] - uyLes[m];
                    uInfRelVec[turbineStartIdBldOnly + nodeId - 1][m] = uInfRel[m];
                }

                // Compute the magnitude of uInfRel. The magnitude is needed for the calculation of the induced velocities (equ. 5.7)
                uInfRelMagn[turbineStartIdBldOnly + nodeId - 1] = mag(uInfRel);
            }
            else
            {
                FAST->setVelocityForceNode(velocity, turbineStartId + nodeId, turbineId);
            }

            nodeId += 1;
            // std::cout << point[0] << " " << point[1] << " " << point[2] << " " << velocity[0] << " " << velocity[1] << " " << velocity[2] << std::endl;
        }
    }

    // Tower
    for (int k = 0; k < nTowerPoints; ++k)
    {
        FAST->getForceNodeCoordinates(point, nodeId, turbineId);
        point = sum(point, multiply(towerSamplingDistance, shaftOrientation));
        velocity = calcVelocity(point, velocityInterpolation);
        FAST->setVelocityForceNode(velocity, turbineStartId + nodeId, turbineId);
        nodeId += 1;
    }

    // This call is required! One can feed velocity at force nodes, but for the Fast internal calculations the velocity is needed at the velocity nodes.
    FAST->interpolateVel_ForceToVelNodes();
}

std::vector<double> AspFast::calcVelocity(const std::vector<double> &point, std::string method)
{
    std::vector<double> velocity;
    if (method == "nearest neighbor")
    {
        velocity = {u[nearestIndex(point, xh, yf, zf)], v[nearestIndex(point, xf, yh, zf)], w[nearestIndex(point, xf, yf, zh)]};
    }

    else if (method == "linear")
    {
        std::vector<int> ijk = nearestIJK(point, xf, yf, zf);
        int i = ijk.at(0);
        int j = ijk.at(1);
        int k = ijk.at(2);

        std::vector<double> cc = {xf[i], yf[j], zf[k]};
        std::vector<double> Ucc = Ucenter(i, j, k);

        std::vector<double> dis = subtract(point, cc);
        std::vector<double> dudx = gradU(i, j, k);

        velocity = sum(Ucc, dot_tv(dudx, dis));
    }
    return velocity;
}

void AspFast::compute_bodyforce_boundingbox()
{
    std::vector<double> nacelle(3, 0.0);
    std::vector<double> turbineTip(3, 0.0);
    std::vector<int> nearest;
    bodyForceExtent = {-1, -1, -1, -1, 0, -1};
    for (int turbineId = 0; turbineId < fi.nTurbinesGlob; ++turbineId)
    {
        int nBladePoints = FAST->get_numForcePtsBlade(turbineId);
        FAST->getForceNodeCoordinates(nacelle, 0, turbineId);
        FAST->getForceNodeCoordinates(turbineTip, nBladePoints, turbineId);
        double radius = mag(subtract(nacelle, turbineTip));
        int ncellsExtent = (radius + projectionRadius) / dx + 2;
        nearest = nearestIJK(nacelle, xf, yf, zf);

        if ((bodyForceExtent[0] > (nearest[0] - ncellsExtent)) || (bodyForceExtent[0] == -1))
            //bodyForceExtent[0] = nearest[0] - ncellsExtent; // max ni
            bodyForceExtent[0] = std::max(0, nearest[0] - ncellsExtent);

        if (bodyForceExtent[1] < (nearest[0] + ncellsExtent))
            //bodyForceExtent[1] = nearest[0] + ncellsExtent;
            bodyForceExtent[1] = std::min(ni, nearest[0] + ncellsExtent);

        if ((bodyForceExtent[2] > (nearest[1] - ncellsExtent)) || (bodyForceExtent[2] == -1))
            //bodyForceExtent[2] = nearest[1] - ncellsExtent;
            bodyForceExtent[2] = std::max(0, nearest[1] - ncellsExtent);

        if (bodyForceExtent[3] < (nearest[1] + ncellsExtent))
            //bodyForceExtent[3] = nearest[1] + ncellsExtent;
            bodyForceExtent[3] = std::min(nj, nearest[1] + ncellsExtent);

        // if ((bodyForceExtent[4] > (nearest[2] - ncellsExtent)) || (bodyForceExtent[4] == -1))
        //     bodyForceExtent[4] = nearest[2] - ncellsExtent;
        if ((bodyForceExtent[4] > (nearest[2] - ncellsExtent)) || (bodyForceExtent[4] == -1))
            bodyForceExtent[4] = std::max(0, nearest[2] - ncellsExtent);

        if (bodyForceExtent[5] < (nearest[2] + ncellsExtent))
            //bodyForceExtent[5] = nearest[2] + ncellsExtent;
            bodyForceExtent[5] = std::min(nk, nearest[2] + ncellsExtent);

//        std::cout << "bodyForceExtent:" << bodyForceExtent[0] << " " << bodyForceExtent[1] << " " << bodyForceExtent[2] << " ";
//        std::cout << bodyForceExtent[3] << " " << bodyForceExtent[4] << " " << bodyForceExtent[5] << std::endl;
    }

    if ((dx > 2 * epsilon) || (dy > 2 * epsilon))
    {
        std::cout << "WARNING: epsilon should be at least 2 * dx, epsilon/dx = " << epsilon / dx << std::endl;
    }
}

void AspFast::calcBodyForce(int turbineId)
{
    simdata->get_variable("fastId").zero();
    simdata->get_variable("fastSpreading").zero();
    simdata->get_variable("fastForce").zero();

    std::vector<double> point(3, 0.0);
    std::vector<double> force(3, 0.0);
    std::vector<double> shaftOrientation(3, 0.0);

    FAST->getHubShftDir(shaftOrientation, turbineId);
    int nBlades = FAST->get_numBlades(turbineId);
    int nBladePoints = FAST->get_numForcePtsBlade(turbineId);
    int nTowerPoints = FAST->get_numForcePtsTwr(turbineId);

    int turbineStartId = 0;
int turbineStartIdBldOnly = 0;
    for (int i = 0; i < turbineId; ++i)
    {
        turbineStartId += FAST->get_numForcePts(i);
// Calculate indices for the filtered ALM arrays (those only include blade points!)
        int nBld = FAST->get_numBlades(i);
        int nBldPts = FAST->get_numForcePtsBlade(i);
        turbineStartIdBldOnly += nBld * nBldPts;
        // std::cout << "Turb " << turbineId << " numForcePts " << FAST->get_numForcePts(turbineId) << std::endl;
    }

    int nodeId = 0;
    if (includeNacelleForce)
    {
        double nacelleEquivalentRadius = sqrt(nacelleArea / M_PI);
        force = multiply(-0.5 * mag(nacelleVelocity) * nacelleArea * nacelleCd, nacelleVelocity);
        // force = multiply(100, force);
        FAST->getForceNodeCoordinates(point, nodeId, turbineId);
        diskBodyForce(point, force, shaftOrientation, nacelleEquivalentRadius);
        /* Distribute the force in shaft direction, 10 points in 1 m
        const int npoints = 10;
        for (int i = 0; i < npoints; ++i)
        {
            diskBodyForce(sum(point, multiply(i/npoints, shaftOrientation)), divide(force, npoints), shaftOrientation, nacelleEquivalentRadius);
        }
        */
    }

    nodeId += 1;
    if (includeRotorForce)
    {
        for (int j = 0; j < nBlades; ++j)
        {
            for (int k = 0; k < nBladePoints; ++k)
            {
                FAST->getForceNodeCoordinates(point, nodeId, turbineId);
                // why is turbineStartId needed? a bug in OpenFAST API?
                FAST->getForce(force, turbineStartId + nodeId, turbineId);
                force = divide(force, airDensity);
                //               totalForce = sum(totalForce, force);
                uniformBodyForce(point, force);

                // Filtered ALM: Store actuator force for the lift calculation at the beginning of the next time step
                // Index for the blade points
                int localForceIdx = j * nBladePoints + k;
                if (filteredALM)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        bladeForceVecDist[turbineStartIdBldOnly + localForceIdx][m] = force[m];
                    }
                    // std::cout << " force: " << force[0] << " " << force[1] << " "<< force[2] << std::endl;
                }

                nodeId += 1;
            }
        }
    }

    if (includeTowerForce)
    {
        nodeId = nBlades * nBladePoints + 1;
        for (int k = 0; k < nTowerPoints; ++k)
        {
            FAST->getForceNodeCoordinates(point, nodeId, turbineId);
            FAST->getForce(force, turbineStartId + nodeId, turbineId);
            force = divide(force, airDensity);
            //            totalForce = sum(totalForce, force);
            std::vector<double> verticalVector = {0, 0, 1};
            double r = 0.5 * FAST->getChord(nodeId, turbineId);
            diskBodyForce(point, force, verticalVector, r);
            nodeId += 1;
            // std::cout << "tower force: " << point[0] << " " << point[1] << " " << point[2] << " " << force[0] << " " << force[1] << " " << force[2] << std::endl;
            // std::cout << "tower force: " << point[2] << " " << force[0] << " " << FAST->getChord(nodeId, turbineId) << std::endl;
        }
    }

    //    std::cout << "Total force structure: "<< totalForce[0] << " " << totalForce[1] << " " << totalForce[2] << std::endl;
}

// Calculates the body force using uniform Gaussian projection.
void AspFast::uniformBodyForce(const std::vector<double> &point, const std::vector<double> &force)
{

    std::vector<double> cc(3, 0.0);                       // Stencil cell center
    std::vector<int> ijk = nearestIJK(point, xf, yf, zf); // Indices of the cell where the point is

    // How many cells the point force influences in each direction (-x, -y, -z, +x, +y, +z)
    int stencilSize = ceil(projectionRadius / dx) + 1;
    std::vector<int> influenceCell; // stencil cell index in i, j, k

    for (int k = 0; k < stencilSize * 2; ++k)
    {
        for (int j = 0; j < stencilSize * 2; ++j)
        {
            for (int i = 0; i < stencilSize * 2; ++i)
            {
                influenceCell = {ijk.at(0) - stencilSize + i, ijk.at(1) - stencilSize + j, ijk.at(2) - stencilSize + k};
                // check that the cell is inside the domain
                if ((influenceCell.at(0) >= 0) && (influenceCell.at(1) >= 0) && (influenceCell.at(2) >= 0) && (influenceCell.at(0) < ni) && (influenceCell.at(1) < nj) && (influenceCell.at(2) < nk))
                {

                    cc = {xf[influenceCell.at(0)], yf[influenceCell.at(1)], zf[influenceCell.at(2)]};

                    int cellId = idx(influenceCell);

                    // distance from point to nearest cell center
                    double dis = mag(subtract(point, cc));

                    // spreading
                    double f = uniformGaussianConstant * exp(-pow(dis / epsilon, 2));

                    bodyForce[cellId][0] += f * force[0];
                    bodyForce[cellId][1] += f * force[1];
                    bodyForce[cellId][2] += f * force[2];

                    fastId[cellId] = 1;
                    fastSpreading[cellId] += f;
                    fastForce[cellId] += sqrt(pow(bodyForce[cellId][0], 2) + pow(bodyForce[cellId][1], 2) + pow(bodyForce[cellId][2], 2));
                }
            }
        }
    }
}

void AspFast::diskBodyForce(const std::vector<double> &point, const std::vector<double> &force, std::vector<double> &axialVec, double r0)
{
    std::vector<double> cc(3, 0.0);
    std::vector<int> ijk = nearestIJK(point, xf, yf, zf);
    int stencilSize = ceil((projectionRadius + r0) / dx) + 1;
    std::vector<int> influenceCell;

    for (int k = 0; k < stencilSize * 2; ++k)
        for (int j = 0; j < stencilSize * 2; ++j)
            for (int i = 0; i < stencilSize * 2; ++i)
            {
                influenceCell = {ijk[0] - stencilSize + i, ijk[1] - stencilSize + j, ijk[2] - stencilSize + k};

                if ((influenceCell[0] >= 0) && (influenceCell[1] >= 0) && (influenceCell[2] >= 0) && (influenceCell[0] < ni) && (influenceCell[1] < nj) && (influenceCell[2] < nk))
                {
                    cc = {xf[influenceCell[0]], yf[influenceCell[1]], zf[influenceCell[2]]};
                    int cellId = idx(influenceCell);

                    std::vector<double> dis = subtract(point, cc);

                    double axialDis = dot(dis, axialVec);
                    axialDis = mag(axialDis);
                    double dr = mag(subtract(dis, multiply(axialDis, axialVec)));

                    double coeff = 1.0 / (sqr(r0) * epsilon * pow(M_PI, 1.5) +
                                          sqr(epsilon) * epsilon * pow(M_PI, 1.5) +
                                          r0 * sqr(epsilon) * sqr(M_PI));

                    double f = 0.1;
                    if (dr <= r0)
                    {
                        f = coeff * exp(-sqr(axialDis / epsilon));
                    }
                    else if (dr > r0)
                    {
                        f = coeff * exp(-sqr(axialDis / epsilon)) * exp(-sqr((dr - r0) / epsilon));
                    }

                    bodyForce[cellId][0] += f * force[0];
                    bodyForce[cellId][1] += f * force[1];
                    bodyForce[cellId][2] += f * force[2];

                    fastId[cellId] = 2;
                    fastSpreading[cellId] += f;
                    fastForce[cellId] += sqrt(pow(bodyForce[cellId][0], 2) + pow(bodyForce[cellId][1], 2) + pow(bodyForce[cellId][2], 2));
                }
            }
}

void AspFast::bodyForce2GraspGrid()
{
    std::valarray<double> bf(0.0, 3);

    simdata->get_variable("ubf").zero();
    simdata->get_variable("vbf").zero();
    simdata->get_variable("wbf").zero();

if (limitProjRange)
    {
    for (int k = bodyForceExtent[4]; k < bodyForceExtent[5]; ++k)
    {
        for (int j = bodyForceExtent[2]; j < bodyForceExtent[3]; ++j)
        {
            for (int i = bodyForceExtent[0]; i < bodyForceExtent[1]; ++i)
            {
                int cellId = idx(i, j, k);
                bf = bodyForce[cellId];

                ubf[idx(i, j, k)] += bf[0] / 2.;
                ubf[idx(i + 1, j, k)] += bf[0] / 2.;

                vbf[idx(i, j, k)] += bf[1] / 2.;
                vbf[idx(i, (j + 1), k)] += bf[1] / 2.;

                wbf[idx(i, j, k)] += bf[2] / 2.;
                wbf[idx(i, j, (k + 1))] += bf[2] / 2.;

                //                totalForce -= dx * dy * dz[k] * bf;
            }
        }
    }
}
    else
    {

        for (int k = 0; k < nk; ++k)
        {
            for (int j = 0; j < nj; ++j)
            {
                for (int i = 0; i < ni; ++i)
                {
                    int cellId = idx(i, j, k);
                    bf = bodyForce[cellId];

                    ubf[idx(i, j, k)] += bf[0] / 2.;
                    ubf[idx(i + 1, j, k)] += bf[0] / 2.;

                    vbf[idx(i, j, k)] += bf[1] / 2.;
                    vbf[idx(i, (j + 1), k)] += bf[1] / 2.;

                    wbf[idx(i, j, k)] += bf[2] / 2.;
                    wbf[idx(i, j, (k + 1))] += bf[2] / 2.;

                    //                totalForce -= dx * dy * dz[k] * bf;
                }
            }
        }
    }

    // Write to GPU and add to velocity tendency
    simdata->get_variable("ubf").cpu2gpu();
    simdata->get_variable("vbf").cpu2gpu();
    simdata->get_variable("wbf").cpu2gpu();

    simdata->get_variable("up").add(simdata->get_variable("ubf"));
    simdata->get_variable("vp").add(simdata->get_variable("vbf"));
    simdata->get_variable("wp").add(simdata->get_variable("wbf"));
}
void AspFast::initializeOutput()
{
    int nTurbines = fi.nTurbinesGlob;
    ASP::Dimension vec("vec", 3, 0);
    simdata->add_coordinate("vec", "", "vector(3)", vec, ASP::Type::INT);

    ASP::Dimension dimTurbine("turbine_FAST", nTurbines, 0);
    simdata->add_coordinate("turbine_FAST", "-", "turbine number", dimTurbine, ASP::Type::INT);

    int nForcePoints = 0;
    for (int i = 0; i < nTurbines; ++i)
    {
        if (FAST->get_numForcePts(i) > nForcePoints)
        {
            nForcePoints = FAST->get_numForcePts(i);
        }
    }
    ASP::Dimension dimFAST("node_FAST", FAST->get_numForcePts(nForcePoints), 0);
    simdata->add_coordinate("node_FAST", "", "FAST node id", dimFAST, ASP::Type::INT);

    ASP::Grid gridTurbine("turbine_FAST", simdata->get_coordinate("turbine_FAST"));
    simdata->add_grid("turbine_FAST", gridTurbine);

    ASP::Grid nodesFAST("node_FAST", simdata->get_coordinate("turbine_FAST"), simdata->get_coordinate("node_FAST"));
    simdata->add_grid("node_FAST", nodesFAST);

    simdata->add_variable("x_base_FAST", "m", "turbine base x", simdata->get_grid("turbine_FAST"), ASP::Type::FLOAT);
    simdata->add_variable("y_base_FAST", "m", "turbine base y", simdata->get_grid("turbine_FAST"), ASP::Type::FLOAT);
    simdata->add_variable("z_base_FAST", "m", "turbine base z", simdata->get_grid("turbine_FAST"), ASP::Type::FLOAT);

    simdata->add_variable("x_hub_FAST", "m", "turbine hub x", simdata->get_grid("turbine_FAST"), ASP::Type::FLOAT);
    simdata->add_variable("y_hub_FAST", "m", "turbine hub y", simdata->get_grid("turbine_FAST"), ASP::Type::FLOAT);
    simdata->add_variable("z_hub_FAST", "m", "turbine hub z", simdata->get_grid("turbine_FAST"), ASP::Type::FLOAT);
    // TODO does this still hold?
    simdata->add_variable("node_type_FAST", "", "Nacelle=0, Tower=1:, Blade=2,3,4", simdata->get_grid("node_FAST"), ASP::Type::INT);

    simdata->add_variable("chord_FAST", "m", "Chord length z", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_x_FAST", "m", "FAST node coordinates x", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_y_FAST", "m", "FAST node coordinates y", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_z_FAST", "m", "FAST node coordinates z", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_fx_FAST", "N", "force x", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE); //, ASP::MemoryMode::cpu);
    simdata->add_variable("_fy_FAST", "N", "force y", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_fz_FAST", "N", "force z", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_uw_FAST", "m/s", "Wind velocity u", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_vw_FAST", "m/s", "Wind velocity v", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_ww_FAST", "m/s", "Wind velocity w", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_ur_FAST", "m/s", "Relative velocity u", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_vr_FAST", "m/s", "Relative velocity v", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_wr_FAST", "m/s", "Relative velocity w", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_orientation0_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation1_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation2_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation3_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation4_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation5_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation6_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation7_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_orientation8_FAST", "", "Blade orientation matrix", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    // Filtered ALM output
    simdata->add_variable("_deltaUyCorr_x", "", "filtered ALM correction", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_deltaUyCorr_y", "", "filtered ALM correction", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_deltaUyCorr_z", "", "filtered ALM correction", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_uIndLes_x", "", "induced velocity by resolved scales", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_uIndLes_y", "", "induced velocity by resolved scales", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_uIndLes_z", "", "induced velocity by resolved scales", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_uIndOpt_x", "", "optimal induced velocity", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_uIndOpt_y", "", "optimal induced velocity", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_uIndOpt_z", "", "optimal induced velocity", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_liftCorr_x", "", "lift resulting from corrected velocity", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_liftCorr_y", "", "lift resulting from corrected velocity", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_liftCorr_z", "", "lift resulting from corrected velocity", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    simdata->add_variable("_uInfBlade_x", "", "corrected inflow velocity in blade reference frame", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_uInfBlade_y", "", "corrected inflow velocity in blade reference frame", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);
    simdata->add_variable("_uInfBlade_z", "", "corrected inflow velocity in blade reference frame", simdata->get_grid("node_FAST"), ASP::Type::DOUBLE);

    // invariants
    for (int turbineId = 0; turbineId < fi.nTurbinesGlob; turbineId++)
    {
        int turbineStartId = 0;
        for (int i = 0; i < turbineId; ++i)
        {
            turbineStartId += FAST->get_numForcePts(i);
        }
        simdata->get_coordinate("turbine_FAST").get_cpu_ptr<int>()[turbineId] = turbineId;
        simdata->get_variable("x_base_FAST").get_cpu_ptr<float>()[turbineId] = fi.globTurbineData[turbineId].TurbineBasePos[0];
        simdata->get_variable("y_base_FAST").get_cpu_ptr<float>()[turbineId] = fi.globTurbineData[turbineId].TurbineBasePos[1];
        simdata->get_variable("z_base_FAST").get_cpu_ptr<float>()[turbineId] = fi.globTurbineData[turbineId].TurbineBasePos[2];
        std::vector<double> hub(3, 0.0);
        FAST->getHubPos(hub, turbineId);

        simdata->get_variable("x_hub_FAST").get_cpu_ptr<float>()[turbineId] = hub[0];
        simdata->get_variable("y_hub_FAST").get_cpu_ptr<float>()[turbineId] = hub[1];
        simdata->get_variable("z_hub_FAST").get_cpu_ptr<float>()[turbineId] = hub[2];

        for (int nodeId = 0; nodeId < FAST->get_numForcePts(turbineId); ++nodeId)
        {
            simdata->get_variable("node_type_FAST").get_cpu_ptr<int>()[turbineStartId + nodeId] = FAST->getForceNodeType(turbineId, turbineStartId + nodeId);
            simdata->get_variable("chord_FAST").get_cpu_ptr<double>()[turbineStartId + nodeId] = FAST->getChord(turbineStartId + nodeId, turbineId);
            simdata->get_coordinate("node_FAST").get_cpu_ptr<int>()[nodeId] = nodeId;
        }
    }

    double time = simdata->get_scalar("timee").as<double>();
    tnextav = time + dtav;
    tnextwrite = time + dtwrite;

    for (auto &varname : ncstat_varnames)
    {
        ASP::Variable &var = simdata->get_variable("_" + varname);

        simdata->add_variable(
            varname, var.get_units(), var.get_longname(), var.get_grid(), var.get_type()); // ASP::Type::DOUBLE);
    }

    for (auto &varname : ncstat_varnames)
    {
        simdata->get_variable(varname).zero();
    }

    nSamples = 0;

    // init output file
    std::string ncfname = simdata->get_scalar("nml_dir").as<std::string>() + "/" + "aspfastOut" + "." + simdata->get_scalar("exp_string").as<std::string>() + ".nc";
    ncwriter = new NcWriter(simdata, ncfname);

    ncwriter->add_file_creation_datetime();
    ncwriter->add_global_attribute("institution", "Whiffle Weather Finecasting Ltd");
    //    ncwriter->add_global_attribute("aspfast_version", VERSION);
    std::vector<std::string> invar_names = {
        "x_base_FAST", "y_base_FAST", "z_base_FAST", "x_hub_FAST", "y_hub_FAST", "z_hub_FAST", "node_type_FAST", "chord_FAST"};
    for (auto &name : invar_names)
        ncwriter->add_variable(name);
    ncwriter->write_variables();

    for (auto &name : ncstat_varnames)
        ncwriter->add_appendable_variable(name);
}

void AspFast::writeOutput(int turbineId)
{
    std::vector<double> point(3, 0.0);
    std::vector<double> force(3, 0.0);
    std::vector<double> urel(3, 0.0);
    std::vector<double> uwind(3, 0.0);
    std::vector<double> orientation(9, 0.0);
    std::vector<double> shaftOrientation(3, 0.0); // Add to output

    double *xFast = simdata->get_variable("_x_FAST").get_cpu_ptr<double>();
    double *yFast = simdata->get_variable("_y_FAST").get_cpu_ptr<double>();
    double *zFast = simdata->get_variable("_z_FAST").get_cpu_ptr<double>();

    double *fxFast = simdata->get_variable("_fx_FAST").get_cpu_ptr<double>();
    double *fyFast = simdata->get_variable("_fy_FAST").get_cpu_ptr<double>();
    double *fzFast = simdata->get_variable("_fz_FAST").get_cpu_ptr<double>();

    double *urFast = simdata->get_variable("_ur_FAST").get_cpu_ptr<double>();
    double *vrFast = simdata->get_variable("_vr_FAST").get_cpu_ptr<double>();
    double *wrFast = simdata->get_variable("_wr_FAST").get_cpu_ptr<double>();

    double *uwFast = simdata->get_variable("_uw_FAST").get_cpu_ptr<double>();
    double *vwFast = simdata->get_variable("_vw_FAST").get_cpu_ptr<double>();
    double *wwFast = simdata->get_variable("_ww_FAST").get_cpu_ptr<double>();

    int turbineStartId = 0;
    for (int i = 0; i < turbineId; ++i)
    {
        turbineStartId += FAST->get_numForcePts(i);
    }

    // turbine start ID excluding tower nodes (for filtered ALM arrays)
    int turbineStartIdBldOnly = 0;
    if (filteredALM)
    {
        for (int i = 0; i < turbineId; ++i)
        {
            // Calculate indices for the filtered ALM arrays (those only include blade points!)
            int nBld = FAST->get_numBlades(i);
            int nBldPts = FAST->get_numForcePtsBlade(i);
            turbineStartIdBldOnly += nBld * nBldPts;
        }
    }

    for (int nodeId = 0; nodeId < FAST->get_numForcePts(turbineId); ++nodeId)
    {
        FAST->getForceNodeCoordinates(point, nodeId, turbineId); // this is not a bug but a FAST 'feature'
        xFast[turbineStartId + nodeId] = point[0];
        yFast[turbineStartId + nodeId] = point[1];
        zFast[turbineStartId + nodeId] = point[2];

        FAST->getForce(force, turbineStartId + nodeId, turbineId);
        fxFast[turbineStartId + nodeId] = force[0];
        fyFast[turbineStartId + nodeId] = force[1];
        fzFast[turbineStartId + nodeId] = force[2];

        FAST->getRelativeVelForceNode(urel, turbineStartId + nodeId, turbineId);
        urFast[turbineStartId + nodeId] = urel[0];
        vrFast[turbineStartId + nodeId] = urel[1];
        wrFast[turbineStartId + nodeId] = urel[2];

        uwind = calcVelocity(point, velocityInterpolation);
        uwFast[turbineStartId + nodeId] = uwind[0];
        vwFast[turbineStartId + nodeId] = uwind[1];
        wwFast[turbineStartId + nodeId] = uwind[2];

        FAST->getForceNodeOrientation(orientation, turbineStartId + nodeId, turbineId);
        for (unsigned int i = 0; i < orientation.size(); ++i)
        {
            simdata->get_variable("_orientation" + std::to_string(i) + "_FAST").get_cpu_ptr<double>()[turbineStartId + nodeId] = orientation.at(i);
        }

        if (filteredALM)
        {
            // The filteredALM arrays only contain blade nodes
            // -> Only change the output arrays for blade nodes (skip nacelle and stop before tower nodes)
            // Calculate end index (last blade point for current turbine)
            int nBldCurrTurb = FAST->get_numBlades(turbineId);
            int nBldPtsCurrTurb = FAST->get_numForcePtsBlade(turbineId);
            int nBldForcePtsCurrTurb = nBldCurrTurb * nBldPtsCurrTurb;

            if ((nodeId > 0) && (nodeId < (nBldForcePtsCurrTurb + 1)))
            {
                simdata->get_variable("_deltaUyCorr_x").get_cpu_ptr<double>()[turbineStartId + nodeId] = deltaUyVec[turbineStartIdBldOnly + nodeId - 1][0];
                simdata->get_variable("_deltaUyCorr_y").get_cpu_ptr<double>()[turbineStartId + nodeId] = deltaUyVec[turbineStartIdBldOnly + nodeId - 1][1];
                simdata->get_variable("_deltaUyCorr_z").get_cpu_ptr<double>()[turbineStartId + nodeId] = deltaUyVec[turbineStartIdBldOnly + nodeId - 1][2];

                simdata->get_variable("_uIndLes_x").get_cpu_ptr<double>()[turbineStartId + nodeId] = uyLesVec[turbineStartIdBldOnly + nodeId - 1][0];
                simdata->get_variable("_uIndLes_y").get_cpu_ptr<double>()[turbineStartId + nodeId] = uyLesVec[turbineStartIdBldOnly + nodeId - 1][1];
                simdata->get_variable("_uIndLes_z").get_cpu_ptr<double>()[turbineStartId + nodeId] = uyLesVec[turbineStartIdBldOnly + nodeId - 1][2];

                simdata->get_variable("_uIndOpt_x").get_cpu_ptr<double>()[turbineStartId + nodeId] = uyOptVec[turbineStartIdBldOnly + nodeId - 1][0];
                simdata->get_variable("_uIndOpt_y").get_cpu_ptr<double>()[turbineStartId + nodeId] = uyOptVec[turbineStartIdBldOnly + nodeId - 1][1];
                simdata->get_variable("_uIndOpt_z").get_cpu_ptr<double>()[turbineStartId + nodeId] = uyOptVec[turbineStartIdBldOnly + nodeId - 1][2];

                simdata->get_variable("_liftCorr_x").get_cpu_ptr<double>()[turbineStartId + nodeId] = bladeLiftVecDist[turbineStartIdBldOnly + nodeId - 1][0];
                simdata->get_variable("_liftCorr_y").get_cpu_ptr<double>()[turbineStartId + nodeId] = bladeLiftVecDist[turbineStartIdBldOnly + nodeId - 1][1];
                simdata->get_variable("_liftCorr_z").get_cpu_ptr<double>()[turbineStartId + nodeId] = bladeLiftVecDist[turbineStartIdBldOnly + nodeId - 1][2];

                simdata->get_variable("_uInfBlade_x").get_cpu_ptr<double>()[turbineStartId + nodeId] = uInfRelVec[turbineStartIdBldOnly + nodeId - 1][0];
                simdata->get_variable("_uInfBlade_y").get_cpu_ptr<double>()[turbineStartId + nodeId] = uInfRelVec[turbineStartIdBldOnly + nodeId - 1][1];
                simdata->get_variable("_uInfBlade_z").get_cpu_ptr<double>()[turbineStartId + nodeId] = uInfRelVec[turbineStartIdBldOnly + nodeId - 1][2];
            }
        }
    }

    for (auto &name : ncstat_varnames)
    {
        // Go through each coordinate of a variable and calculate the grid size from them.
        std::vector<ASP::Coordinate *> coordinates = simdata->get_variable(name).get_grid().get_coordinates();
        int n_node = 1;
        for (unsigned int i = 0; i < coordinates.size(); ++i)
        {
            n_node *= coordinates[i]->get_inner_size();
        }
        stat_wadd(
            simdata->get_variable(name).get_cpu_ptr<double>(),
            simdata->get_variable("_" + name).get_cpu_ptr<double>(),
            n_node);
    }
}

void AspFast::stat_wadd(double *average, const double *sample, const unsigned int N)
{
    for (size_t n = 0; n < N; n++)
    {
        average[n] = (nSamples * average[n] + sample[n]) / (nSamples + 1.0);
    }
}

std::vector<double> AspFast::Ucenter(int i, int j, int k)
{
    double ucc = (u[idx(i, j, k)] + u[idx(i + 1, j, k)]) / 2;
    double vcc = (v[idx(i, j, k)] + v[idx(i, j + 1, k)]) / 2;
    double wcc = (w[idx(i, j, k)] + w[idx(i, j, k + 1)]) / 2;
    return std::vector<double>{ucc, vcc, wcc};
}

std::vector<double> AspFast::gradU(int i, int j, int k)
{
    // Diagonal
    double dudx = (u[idx(i + 1, j, k)] - u[idx(i, j, k)]) / dx;
    double dvdy = (v[idx(i, j + 1, k)] - v[idx(i, j, k)]) / dy;
    double dwdz = (w[idx(i, j, k + 1)] - w[idx(i, j, k)]) / dz[k];

    // Off diagonal
    double dvdx = (v[idx(i + 1, j, k)] - v[idx(i - 1, j, k)] +
                   v[idx(i + 1, j + 1, k)] - v[idx(i - 1, j + 1, k)]) /
                  4 / dx;

    double dwdx = (w[idx(i + 1, j, k)] - w[idx(i - 1, j, k)] +
                   w[idx(i + 1, j, k + 1)] - w[idx(i - 1, j, k + 1)]) /
                  4 / dx;

    double dudy = (u[idx(i, j + 1, k)] - u[idx(i, j - 1, k)] +
                   u[idx(i + 1, j + 1, k)] - u[idx(i + 1, j - 1, k)]) /
                  4 / dy;

    double dwdy = (w[idx(i, j + 1, k)] - w[idx(i, j - 1, k)] +
                   w[idx(i, j + 1, k + 1)] - w[idx(i, j - 1, k + 1)]) /
                  4 / dy;

    double dudz;
    double dvdz;
    if (k != 0)
    {
        dudz = (u[idx(i, j, k + 1)] - u[idx(i, j, k - 1)] +
                u[idx(i + 1, j, k + 1)] - u[idx(i + 1, j, k - 1)]) /
               4 / dz[k];
        dvdz = (v[idx(i, j, k + 1)] - v[idx(i, j, k - 1)] +
                v[idx(i, j + 1, k + 1)] - v[idx(i, j + 1, k - 1)]) /
               4 / dz[k];
    }
    else
    {
        // Assume zero velocity at k[0] (root of the tower)
        dudz = (u[idx(i, j, k)] + u[idx(i + 1, j, k)]) / dz[k];
        dvdz = (v[idx(i, j, k)] + v[idx(i, j + 1, k)]) / dz[k];
    }

    return {dudx, dudy, dudz,
            dvdx, dvdy, dvdz,
            dwdx, dwdy, dwdz};
}

int AspFast::nearestIndex1(float *x, float loc, int n)
{
    int index = 0;
    for (int i = 1; i < n; i++)
    {
        if (fabs(x[i] - loc) < fabs(x[index] - loc))
            index = i;
    }
    return index;
}

std::vector<int> AspFast::nearestIJK(const std::vector<double> &point, float *x, float *y, float *z)
{
    int indexi = nearestIndex1(x, point[0], ni);
    int indexj = nearestIndex1(y, point[1], nj);
    int indexk = nearestIndex1(z, point[2], nk);
    return std::vector<int>{indexi, indexj, indexk};
}

int AspFast::nearestIndex(const std::vector<double> &point, float *x, float *y, float *z)
{
    int indexi = nearestIndex1(x, point[0], ni);
    int indexj = nearestIndex1(y, point[1], nj);
    int indexk = nearestIndex1(z, point[2], nk);

    return idx(indexi, indexj, indexk);
}

int AspFast::idx(std::vector<int> ijk)
{
    int index = ijk[0] + ijk[1] * ni + ijk[2] * ni * nj;
    return index;
}

int AspFast::idx(int i, int j, int k)
{
    int index = i + j * ni + k * ni * nj;
    return index;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// filtered ALM function: All references regarding equations refer to:
// Luis A. Martinez-Tossas et al., "Filtered lifting line theory and application to the actuator line model", J. Fluid Mech. (2019), vol 863, pp. 269-292

void AspFast::calcFilteredAlmVelCorr(int turbineId)
{

    std::vector<double> point_zk(3, 0.0);
    std::vector<double> point_zi(3, 0.0);
    std::vector<double> pointA(3, 0.0);
    std::vector<double> pointB(3, 0.0);
    std::vector<double> velocityRel(3, 0.0);
    std::vector<double> force(3, 0.0);
    double dr = 0;
    double zdist;
    double epsilonOpt;
    double fu = 0.1; // Relaxation factor

    // Determine starting index for the current turbine
    int turbineStartId = 0;
    for (int i = 0; i < turbineId; ++i)
    {
        // Calculate indices for the filtered ALM arrays (those only include blade points!)
        int nBld = FAST->get_numBlades(i);
        int nBldPts = FAST->get_numForcePtsBlade(i);
        turbineStartId += nBld * nBldPts;
    }

    // Indices for the current turbine
    int nBld_Loc = FAST->get_numBlades(turbineId);
    int nBldPts_Loc = FAST->get_numForcePtsBlade(turbineId);

    // JFM 2019 (equ. 5.3): Compute lift force vector using the new relative velocity uInfRel at the actuator point
    for (int j = 0; j < nBld_Loc; ++j)
    {
        for (int k = 0; k < nBldPts_Loc; ++k)
        {
            // Index for local blade actuator point
            int localForceIdx = j * nBldPts_Loc + k;

            // Calculate actuator point spacing in spanwise direction
            if ((j == 0) && (k == 0))
            {
                FAST->getForceNodeCoordinates(pointA, 1 + localForceIdx, turbineId); // +1 to skip the nacelle node
                FAST->getForceNodeCoordinates(pointB, 2 + localForceIdx, turbineId);
                dr = mag(subtract(pointA, pointB));
                // std::cout << " pointA: " << pointA[0] << " " << pointA[1] << " "<< pointA[2] << std::endl;
                // std::cout << " pointB: " << pointB[0] << " " << pointB[1] << " "<< pointB[2] << std::endl;
            }

            // Get the relative velocity uInfRel at the current actuator point (excluding the induced velocity resolved by the LES) and the total actuator force
            // Lift is defined perpendicular to this velocity
            for (int m = 0; m < 3; ++m)
            {
                velocityRel[m] = uInfRelVec[turbineStartId + localForceIdx][m];
                force[m] = bladeForceVecDist[turbineStartId + localForceIdx][m];
            }

            // Determine lift vector by projecting force vector parallel to uInfRelVec and subtracting this component
            double fv = dot(force, velocityRel);
            double vmag2 = dot(velocityRel, velocityRel);

            // The most inner and outer point are located directly on the edge of the blade. Consequently their spanwise extent is only 0.5*dr.
            if (k == 0)
            {
                for (int m = 0; m < 3; ++m)
                {
                    bladeLiftVecDist[turbineStartId + localForceIdx][m] = (force[m] - velocityRel[m] * fv / vmag2) / (0.5 * dr);
                }
            }
            else if (k == (nBldPts_Loc - 1))
            {
                for (int m = 0; m < 3; ++m)
                {
                    bladeLiftVecDist[turbineStartId + localForceIdx][m] = (force[m] - velocityRel[m] * fv / vmag2) / (0.5 * dr);
                }
            }
            else
            {
                for (int m = 0; m < 3; ++m)
                {
                    bladeLiftVecDist[turbineStartId + localForceIdx][m] = (force[m] - velocityRel[m] * fv / vmag2) / dr;
                }
            }
        }
    }

    // JFM 2019 (equ. 5.4 & 5.5a,b): Compute gradient of lift force vector (i.e. only the finite differences)
    for (int j = 0; j < nBld_Loc; ++j)
    {
        for (int k = 0; k < nBldPts_Loc; ++k)
        {
            int localIdx = j * nBldPts_Loc + k;
            for (int m = 0; m < 3; m++)
            {
                if (k == 0)
                {
                    bladeGradLiftVecDist[turbineStartId + localIdx][m] = 1.0 * bladeLiftVecDist[turbineStartId + localIdx][m];
                }
                else if (k == (nBldPts_Loc - 1))
                {
                    bladeGradLiftVecDist[turbineStartId + localIdx][m] = -1.0 * bladeLiftVecDist[turbineStartId + localIdx][m];
                }
                else
                {
                    bladeGradLiftVecDist[turbineStartId + localIdx][m] = 0.5 * (bladeLiftVecDist[turbineStartId + localIdx + 1][m] - bladeLiftVecDist[turbineStartId + localIdx - 1][m]);
                }
            }
            // std::cout << "FD lift mag:   " << bladeGradLiftDist[turbineStartId+localIdx] << std::endl;
        }
    }

    // Loop over all blades and blade points
    for (int j = 0; j < nBld_Loc; ++j)
    {
        for (int k = 0; k < nBldPts_Loc; ++k)
        {
            std::vector<double> uyLes(3, 0.0);
            std::vector<double> uyOpt(3, 0.0);

            // JFM 2019 (equ. 5.7): Compute the induced velocity both for les and optimal epsilon (Loop across all influencing points (skipping the case k==i))
            int bldPtIdx1 = j * nBldPts_Loc + k;
            FAST->getForceNodeCoordinates(point_zk, 1 + bldPtIdx1, turbineId); // +1 to skip the nacelle node

            // This is the sum of equ. 5.7 (the paper is erroneous here! Uinf has to be inside the sum!)
            for (int i = 0; i < nBldPts_Loc; ++i)
            {
                int bldPtIdx2 = j * nBldPts_Loc + i;

                if (i != k)
                {

                    // Distance between current blade point k and the ith blade point influencing the velocity correction at point k
                    FAST->getForceNodeCoordinates(point_zi, 1 + bldPtIdx2, turbineId); // +1 to skip the nacelle node
                    zdist = mag(subtract(point_zk, point_zi));
                    if (k < i)
                    {
                        zdist *= -1.0;
                    }

                    // Induced velocity
                    double factorLes = (1.0 - exp(-pow(zdist / epsilon, 2))) / (4 * M_PI * zdist); // LES
                    epsilonOpt = 0.25 * localChordDist[k];
                    double factorOpt = (1.0 - exp(-pow(zdist / epsilonOpt, 2))) / (4 * M_PI * zdist); // optimal

                    for (int m = 0; m < 3; m++)
                    {
                        // LES
                        // This should be -= since the OpenFast force is the reaction force to the lift force (but the formula is written for the lift force with =+)
                        uyLes[m] -= bladeGradLiftVecDist[turbineStartId + bldPtIdx2][m] * factorLes * (-1.0 / uInfRelMagn[turbineStartId + bldPtIdx2]);

                        // Optimal
                        uyOpt[m] -= bladeGradLiftVecDist[turbineStartId + bldPtIdx2][m] * factorOpt * (-1.0 / uInfRelMagn[turbineStartId + bldPtIdx2]);
                        // std::cout << "UrotFrame Magn: " << uInfRotMagn[turbineStartId + bldPtIdx2] << std::endl;
                    }
                }
            }

            // Update the difference between induced velocities using under-relaxation and store the current dUy for the next time step
            for (int m = 0; m < 3; m++)
            {
                deltaUyVec[turbineStartId + bldPtIdx1][m] = fu * (uyOpt[m] - uyLes[m]) + (1 - fu) * deltaUyVecStash[turbineStartId + bldPtIdx1][m];
                deltaUyVecStash[turbineStartId + bldPtIdx1][m] = deltaUyVec[turbineStartId + bldPtIdx1][m];
                uyLesVec[turbineStartId + bldPtIdx1][m] = uyLes[m];
                uyOptVec[turbineStartId + bldPtIdx1][m] = uyOpt[m];
            }
        }
    }
}
