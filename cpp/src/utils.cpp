#include <iostream>
#include <vector>
#include <cmath>
#include "utils.h"

std::vector<double> divide(const std::vector<double> &v1, const double &d1)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result[i] = v1[i] / d1;
    }
    return result;
}

std::vector<double> sum(const std::vector<double> &v1, const std::vector<double> &v2)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result[i] = v1[i] + v2[i];
    }
    return result;
}

std::vector<double> sum(const std::vector<double> &v1, const double &d)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result[i] = v1[i] + d;
    }
    return result;
}

std::vector<double> subtract(const std::vector<double> &v1, const std::vector<double> &v2)
{

    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result[i] = v1[i] - v2[i];
    }
    return result;
}

std::vector<double> subtract(const std::vector<double> &v1, const double &d)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result[i] = v1[i] - d;
    }
    return result;
}

std::vector<double> subtract(const double &d, const std::vector<double> &v1)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result[i] = d - v1[i];
    }
    return result;
}

std::vector<double> multiply(const double &d, const std::vector<double> &v1)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result[i] = d * v1[i];
    }
    return result;
}
double sqr(const double &f1)
{
    return f1 * f1;
}

double mag(const double &f1)
{
    return abs(f1);
}

double mag(const std::vector<double> &v1)
{
    double mag = 0;
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        mag += v1[i] * v1[i];
    }
    mag = sqrt(mag);

    return mag;
}

double dot(const std::vector<double> &v1, const std::vector<double> &v2)
{
    double result = 0;
    for (unsigned int i = 0; i < v1.size(); ++i)
    {
        result += v1[i] * v2[i];
    }
    return result;
}

std::vector<double> normalize(const std::vector<double> &v1)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    double magnitude = mag(v1);
    result = divide(v1, magnitude);

    return result;
}

std::vector<double> dot_tv(const std::vector<double> &t, const std::vector<double> &v)
{
    std::vector<double> result = std::vector<double>(3, 0.0);
    for (unsigned int i = 0; i < 3; ++i)
    {
        for (unsigned int j = 0; j < 3; ++j)
        {

            result[i] += t[j + 3 * i] * v[j];
        }
    }
    return result;
}

std::vector<double> cross(const std::vector<double> &v1, const std::vector<double> &v2)
{
    std::vector<double> result = std::vector<double>(3, 0.0);

    result[0] = v1[1] * v2[2] - v1[2] * v2[1];
    result[1] = -v1[0] * v2[2] + v1[2] * v2[0];
    result[2] = v1[0] * v2[1] - v1[1] * v2[0];
    return result;
}
