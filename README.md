# ASPFAST

## Build and test

1. Clone the repository
```
git clone git@gitlab.com:whiffle-public/aspfast.git
```

2. Login to Whiffle registry

```
docker login registry.whffl.nl
```

3. Build the aspfast plugin

```
export ASPIRE_LICENSE_KEY=<your licence key>
docker run -e ASPIRE_LICENSE_KEY -v $(pwd):/app -w /app registry.whffl.nl/aspkit-license/aspkit-devel:7.7.0 cmake . -B build -DASPFAST_MASTER_PROJECT=ON
docker run -e ASPIRE_LICENSE_KEY -v $(pwd):/app -w /app registry.whffl.nl/aspkit-license/aspkit-devel:7.7.0 cmake --build build
```

4. Run the minimal test case

```
docker run -e ASPIRE_LICENSE_KEY -v $(pwd):/app -w /app -e ASPFAST_LIB=build/cpp registry.whffl.nl/aspkit-license/aspkit-devel:7.7.0 aspire tests/system/load_plugin/graspIn.000.nml
```

## Developing

1. Create your own branch 

```
git checkout -b my-new-feature
```
2. Make your changes in the source code
3. Build the code
5. Run aspire with the built aspfast version (add -d parameter to run in the background)

```
docker run -e ASPIRE_LICENSE_KEY -v $(pwd):/app -w /app -e ASPFAST_LIB=build/cpp registry.whffl.nl/aspkit-license/aspkit-devel:7.7.0  aspire tests/nrel5mw_openbc/graspIn.000.nml
```

6. Once the new feature is ready, make a merge request